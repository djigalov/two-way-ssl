
const https = require('https');
let path = require('path');
let fs = require('fs');
let express = require('express');
let app = express();

let port = 443;
let options = {
    key : fs.readFileSync('D:\\symphony\\apache-tomcat-8.5.34\\conf\\encryption\\server-key.pem'),
    cert : fs.readFileSync('D:\\symphony\\apache-tomcat-8.5.34\\conf\\encryption\\server-crt.pem'),
    ca : fs.readFileSync('D:\\symphony\\apache-tomcat-8.5.34\\conf\\encryption\\ca-crt.pem'),
    requestCert : false,
    rejectUnauthorized : false
};



app.get('/get-data/get-file/file.json',function (req, res, next) {
    console.log(`At ${new Date()} got ${req.method}:${req.url} request`);
    sendFile('file.json', res);
    next();
});

let listener = https.createServer(options, app).listen(port, function () {
    console.log('Express HTTPS server listening on port ' + listener.address().port);
});

let sendFile = (fileName, res) => {
    let filePath = path.join(__dirname,'resources','server', fileName);
    let file = fs.createReadStream(filePath, {encoding:"UTF-8"});

    res.writeHead(200, {
        'Content-Type': 'application/json',
        'Content-Length': fs.statSync(filePath).size

    });
    file.on('error', ()=>{
        res.statusCode = 500;
        res.end('Server Error');
    }).pipe(res);
};



